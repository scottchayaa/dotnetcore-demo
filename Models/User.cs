﻿using System;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace dotnetcore_demo.Models
{
    public partial class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50, ErrorMessage = "name should not over 50 length.")]
        public string name { get; set; }
        public string phone { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}
