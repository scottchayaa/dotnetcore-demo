﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using dotnetcore_demo.Models;

#nullable disable

namespace dotnetcore_demo.Models
{
    public partial class DemoContext : DbContext
    {
        public DemoContext()
        {
        }

        public DemoContext(DbContextOptions<DemoContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}
